<?php
	// header senden
	header("Content-Type: image/svg+xml");
	echo '<?xml version="1.0" standalone="no"?>';
?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg">
<?php
	// Standardwerte setzen
	$jetzt = getdate();
	$std = $jetzt[hours];
	$min = $jetzt[minutes];

	$color = "lightgrey";
	if(isset($_GET['color']))
		$color=$_GET['color'];

	$opacity = 100;
	if(isset($_GET['opacity']))
		$opacity=$_GET['opacity']/100;

	if((bool)preg_match('/^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/',($_GET['time']))){
	 $time = explode(':',$_GET['time']);
	 $min = $time[1];
	 $std = $time[0];
	} 

	echo '<title>'.$std.':'.sprintf("%02d",$min).'</title>\n\r';
	echo '<desc>'.$std.':'.sprintf("%02d",$min).'</desc>\n\r';
	// Radius
	$r = 30;
	if(isset($_GET['size']))
		$r=$_GET['size'];
	// Mittelpunkt
	$m = $r*1.1;
	// Laenge Minutenzeiger
	$l_min = 0.80*$r;
	// Laenge Stundenzeiger
	$l_std = 0.50*$r;
	// breite der Zeiger
	$b_z = 0.1*$r; // 0.08
	// breite+ des aeusseren Kreises
	$b_k = 1.5*$b_z;

	// minutenzeiger
	$min_X = $m+$l_min*cos(($min/60)*2*pi()-pi()/2);
	$min_Y = $m+$l_min*sin(($min/60)*2*pi()-pi()/2);
	echo '<line x1="'.$m.'" y1="'.$m.'" x2="'.$min_X.'" y2="'.$min_Y.'" stroke="'.$color.'" stroke-width="'.$b_z.'" stroke-linecap="round" stroke-opacity="'.$opacity.'"/>\n\r';

	// stundenzeiger
	$std_min = 60*$std+$min;
	$std_X = $m+$l_std*cos(($std_min/720)*2*pi()-pi()/2);
	$std_Y = $m+$l_std*sin(($std_min/720)*2*pi()-pi()/2);
	echo '<line   x1="'.$m.'" y1="'.$m.'" x2="'.$std_X.'" y2="'.$std_Y.'"  stroke="'.$color.'" stroke-width="'.$b_z.'" stroke-linecap="round" stroke-opacity="'.$opacity.'"/>\n\r'; 

	//kreise
	echo '<circle cx="'.$m.'" cy="'.$m.'"  r="'.$r.'" stroke="'.$color.'" stroke-width="'.$b_k.'" style="fill:none;" stroke-opacity="'.$opacity.'"/>\n\r';
?>
</svg>