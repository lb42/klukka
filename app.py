import os

from flask import Flask, render_template, request, make_response, send_file
import datetime
import time

import math
import string
from decimal import Decimal

import urllib2
import simplejson
from xml.dom.minidom import parseString

from werkzeug.contrib.cache import SimpleCache

import sys

app = Flask(__name__)
cache = SimpleCache()

@app.route('/help')
def help():
        return render_template('help.html')

@app.route('/')
def clock():
	try:	radius = Decimal(request.values['size'])#/Decimal(2)
	except:	radius = Decimal(30)
	
	try:	color = request.values['color']
	except:	color = 'aaaaaa'
	if not ishex(color) or not len(color) == 6: 
		app.abort(400)

	try:	opacity = float(request.values['opacity'])/100
	except:	opacity = 1
	try:
		time_str 	= str(request.values['time'])
		now 		= datetime.datetime.fromtimestamp(time.mktime(time.strptime(time_str, '%H:%M')))
	except:
		now 		= datetime.datetime.utcnow()
		try:
			offset = float(request.values['timezone'])
		except:
			# get timezone
			url 		= "http://api.ipinfodb.com/v3/ip-city/?key=b8e2acaf657a0615fcab3ab2801c5dcbfba0f4905692fa23800c417e2e3badf3&format=json"
			json		= urllib2.urlopen(url).read()
			parsed_data = simplejson.loads(json)
			latitude 	= parsed_data['latitude']
			longitude	= parsed_data['longitude']
			url 		= "http://www.earthtools.org/timezone/%s/%s" % (parsed_data['latitude'], parsed_data['longitude'])
			#url 		= "http://www.earthtools.org/timezone/35.7/51.416667" # Teheran
			#url 		= "http://www.earthtools.org/timezone/10.5/-66.933333" # Caracas
			xml			= urllib2.urlopen(url).read()
			dom 		= parseString(xml)
			offset_tag	= dom.getElementsByTagName('offset')[0].toxml()
			offset		= float(offset_tag.replace('<offset>','').replace('</offset>',''))
			dst_tag		= dom.getElementsByTagName('dst')[0].toxml()
			dst			= dst_tag.replace('<dst>','').replace('</dst>','')
			if dst == 'True': offset += 1
		
		hour 		= int(str(offset).split('.')[0])
		minute 		= int(str(offset).split('.')[1])*6
		offset 		= datetime.timedelta(hours=hour,minutes=minute)
		now 		+= offset
		time_str	= '%s:%s' % (str(now.hour).zfill(2),str(now.minute).zfill(2))
	
	try:	format = request.values['format']
	except:	format = 'svg'
	if format not in ['svg','png']:
		app.abort(400)

	# radius, time, color, opacity
	parameters = '%s;%s;%s;%s;%s' % (radius,time_str,color,opacity,format)

	response = cache.get(parameters)
	if response is None:
		print 'add new pic to cache'
		r = radius * Decimal('1.1') 
		# Mittelpunkt
		m = r * Decimal('1.1')
		# Laenge Minutenzeiger
		l_min = Decimal('0.80') * r
		# Laenge Stundenzeiger
		l_std = Decimal('0.50') * r
		# breite der Zeiger
		b_z = Decimal('0.1') * r # 0.08
		# breite+ des aeusseren Kreises
		b_k = Decimal('1.5') * b_z
		# minutenzeiger
		min_X = m + l_min * Decimal(str(math.cos((Decimal(now.minute) / Decimal(60)) * Decimal(2) * Decimal(str(math.pi)) - Decimal(str(math.pi)) / Decimal(2))));
		min_Y = m + l_min * Decimal(str(math.sin((Decimal(now.minute) / Decimal(60)) * Decimal(2) * Decimal(str(math.pi)) - Decimal(str(math.pi)) / Decimal(2))));
		# stundenzeiger
		std_min = Decimal(60) * Decimal(now.hour) + Decimal(now.minute);
		std_X = m + l_std * Decimal(str(math.cos((std_min / Decimal(720)) * Decimal(2) * Decimal(str(math.pi)) - Decimal(str(math.pi)) / Decimal(2))));
		std_Y = m + l_std * Decimal(str(math.sin((std_min / Decimal(720)) * Decimal(2) * Decimal(str(math.pi)) - Decimal(str(math.pi)) / Decimal(2))));

		svg = render_template('clock.xml', size=(2*r+2*b_k), time_str=time_str, m=m, r=r, b_k=b_k, b_z=b_z, min_X=min_X, min_Y=min_Y, std_X=std_X, std_Y=std_Y, color=color, opacity=opacity)

		if format == 'svg':
			response = make_response(svg)
			response.headers['Content-Type'] = 'image/svg+xml'
		else:
			svg_file = open('time.svg', 'w')
			svg_file.write(svg)
			svg_file.close()
			os.system('convert -background none -compress Lossless time.svg time.png')
			
			png_file = open('time.png','r')
			png = png_file.read()
			png_file.close()

			response = make_response(png)
			response.headers['Content-Type'] = 'image/png'
			#return send_file('time.png')
			
			#kwargs 				= {'dpi': float(96)}
			#bytes_stdout 		= StringIO()
			#kwargs['write_to'] 	= bytes_stdout
			#cairosvg.surface.PNGSurface.convert(bytestring=svg,**kwargs)

			#response = make_response(bytes_stdout.getvalue())
			#response.headers['Content-Type'] = 'image/png'
			#return response
		
		cache.set(parameters,response,timeout = 9999)
	
	return response

def ishex(s): 
  for c in s: 
    if not c in string.hexdigits: return False 
  return True 

if __name__ == '__main__':
	# Bind to PORT if defined, otherwise default to 5000.
	port = int(os.environ.get('PORT', 5001))
	app.debug = True
	app.run(host='0.0.0.0', port=port)